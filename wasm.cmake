# cmake -DCMAKE_FIND_ROOT_PATH=/Users/lpotter/Qt-com/5.15.1/wasm_32/ -DCMAKE_TOOLCHAIN_FILE=~/development/wasm.cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON ~/src/wasm-cmake-quickapp -DCMAKE_BUILD_TYPE=Debug

include(/Users/lpotter/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake)

# see https://github.com/emscripten-core/emscripten/blob/master/src/settings.js

## todo ^dynamic DCMAKE_TOOLCHAIN_FILE
## -device-option EMSCRIPTEN_ASYNCIFY=1   QMAKE_LFLAGS_DEBUG  += -Os
## QMAKE_WASM_TOTAL_MEMORY
## WASM_SOURCE_MAP_BASE
## emcc ver
## QMAKE_DISTCLEAN

set(CMAKE_VERBOSE_MAKEFILE ON)

if (QT_FEATURE_opengl3)
    add_link_options("SHELL:-s FULL_ES3=1")
    add_link_options("SHELL:-s MAX_WEBGL_VERSION=2")
    add_link_options("SHELL:-s WEBGL2_BACKWARDS_COMPATIBILITY_EMULATION=1")
else()
    add_link_options("SHELL:-s FULL_ES2=1")
    add_link_options("SHELL:-s MAX_WEBGL_VERSION=1")
endif()
add_link_options("SHELL:-s EXIT_RUNTIME=1")
add_link_options("SHELL:-s ERROR_ON_UNDEFINED_SYMBOLS=1")
add_link_options("SHELL:-s EXTRA_EXPORTED_RUNTIME_METHODS=[UTF16ToString,stringToUTF16]")
add_link_options(--bind)
add_link_options("SHELL:-s FETCH=1")

#add_link_options("SHELL:-s MAXIMUM_MEMORY=1GB") # required when combining USE_PTHREADS with ALLOW_MEMORY_GROWTH

if (QT_FEATURE_exceptions)
    add_link_options("SHELL:-s DISABLE_EXCEPTION_CATCHING=0")
endif()

# add_link_options("SHELL:-s WASM_OBJECT_FILES=1") ## now default

if (QT_FEATURE_thread)
    add_link_options("SHELL:-s USE_PTHREADS=1")

    # POOL_SIZE = $$QMAKE_WASM_PTHREAD_POOL_SIZE
    add_link_options("SHELL:-s PTHREAD_POOL_SIZE=$$POOL_SIZE")
    message("Setting PTHREAD_POOL_SIZE to" $$POOL_SIZE)

    # TOTAL_MEMORY = 1GB
    # if QMAKE_WASM_TOTAL_MEMORY
    # TOTAL_MEMORY = $$QMAKE_WASM_TOTAL_MEMORY
    add_link_options("SHELL:-s TOTAL_MEMORY=$$TOTAL_MEMORY")
else()
    add_link_options("SHELL:-s ALLOW_MEMORY_GROWTH=1")
endif()

### debug add_compile_options
if (CMAKE_BUILD_TYPE STREQUAL "Debug")
message("adding funky debug thingies")
    ## set (CMAKE_EXE_LINKER_FLAGS -g4 )
    set(GCC_EMCC_CODE_MAPS_FLAGS "-g4")
    set(CMAKE_CXX_FLAGS_DEBUG  "${CMAKE_CXX_FLAGS} ${GCC_EMCC_CODE_MAPS_FLAGS}")
    set(CMAKE_C_FLAGS_DEBUG  "${CMAKE_C_FLAGS} ${GCC_EMCC_CODE_MAPS_FLAGS}")

    add_link_options("SHELL:-s DEMANGLE_SUPPORT=1")
    add_link_options("SHELL:-s GL_DEBUG=1")
    add_link_options("SHELL:-s ASSERTIONS=2")
    add_link_options("SHELL:--profiling-funcs")

    # add_link_options("SHELL:-s LIBRARY_DEBUG=1") # print out library calls, verbose
    # add_link_options("SHELL:-s SYSCALL_DEBUG=1") # print out sys calls, verbose
    # add_link_options("SHELL:-s FS_LOG=1") # print out filesystem ops, verbose
    # add_link_options("SHELL:-s SOCKET_DEBUG") # print out socket,network data transfer
endif()

# Also search for packages beneath filesystem root (in addition to /emsdk_portable/sdk/system)
# list(APPEND CMAKE_FIND_ROOT_PATH "/")


 function(target_link_libraries target)
    # copy in Qt HTML/JS launch files
    set(APPNAME ${target})
    configure_file("${_qt5Core_install_prefix}/plugins/platforms/wasm_shell.html"
                   "${target}.html")
    configure_file("${_qt5Core_install_prefix}/plugins/platforms/qtloader.js"
                   qtloader.js COPYONLY)
    configure_file("${_qt5Core_install_prefix}/plugins/platforms/qtlogo.svg"
                   qtlogo.svg COPYONLY)
endfunction()

